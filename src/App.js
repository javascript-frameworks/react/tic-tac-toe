import { useState } from "react";
import { Board } from "./Board";


export default function Game() {
  const [currentMove, setCurrentMove] = useState(0);
  const xIsNext = currentMove % 2 === 0;
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const currentSquares = history[currentMove];
  const [sortingOrderAsc, setSortingOrderAsc] = useState(true);
  const orderClass = sortingOrderAsc ? "list-order-asc" : "list-order-desc"

  function handlePlay(nextSquares) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);
  }

  function jumpTo(nextMove) {
    setCurrentMove(nextMove);
  }

  function sortHistory() {
    setSortingOrderAsc(!sortingOrderAsc)
  }

  const moves = history.map((squares, move) => {
    let description;
    if (move === currentMove) {
      return (
        <li key={move}>
          <span>You are at move #{currentMove}</span>
        </li>
      )
    }
    if (move > 0) {
      description = 'Go to move #' + move;
    }
    else {
      description = 'Go to game start';
    }
    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{description}</button>
      </li>
    )
  });

  return (
    <div className="game">
      <div className="game-board">
        <Board xIsNext={xIsNext} squares={currentSquares} onPlay={handlePlay} currentMove={currentMove} />
      </div>

      <div className="game-info">
        <button onClick={() => sortHistory()}>Sort</button>
        <ol className={orderClass}>{moves}</ol>
      </div>
    </div>
  );
}