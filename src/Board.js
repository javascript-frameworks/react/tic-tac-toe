import { Square } from "./Square";

export function Board({ xIsNext, squares, onPlay, currentMove }) {
    const winner = calculateWinner(squares);
    let winnerSequence = [];
    let status;

    if (winner) {
        status = "Winner: " + winner.winner;
        winnerSequence = winner.winnerSequence;
    } else {
        winnerSequence = []
        if (currentMove == 9) {
            status = "Draw"
        }
        else {
            status = "Next player: " + (xIsNext ? "X" : "O");
        }
    }

    function handleClick(i) {
        if (calculateWinner(squares) || squares[i]) {
            return;
        }
        const nextSquares = squares.slice();
        if (xIsNext) {
            nextSquares[i] = "X";
        } else {
            nextSquares[i] = "O";
        }
        onPlay(nextSquares);
    }

    const board = getBoard();
    return (
        <>
            <div className="status">{status}</div>
            {board}
        </>
    );

    function getBoard() {
        let rows = []
        for (let i = 0; i < 3; i++) {
            let row = getRow(i);
            rows.push(
                <div key={i} className="board-row">
                    {row}
                </div>
            );
        }
        return (
            <>
                {rows}
            </>
        );
    }

    function getRow(rowId) {
        let items = []
        for (let j = 0; j < 3; j++) {
            items.push(<Square highlightWonSquare={winnerSequence.includes(j + rowId * 3)} key={j + rowId * 3} value={squares[j + rowId * 3]} onSquareClick={() => handleClick(j + rowId * 3)} />);
        }
        return (<>{items}</>);
    }
}


function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return { winnerSequence: lines[i], winner: squares[a] }
        }
    }
    return null;
}