export function Square({ value, onSquareClick, highlightWonSquare }) {
    let cssClassNames = "square";
    if (highlightWonSquare) {
        cssClassNames += " highlight"
    }
    return <button className={cssClassNames} onClick={onSquareClick}>{value}</button>;
}